package com.javatechie.rabbitmq.demo;

import com.javatechie.rabbitmq.demo.config.MQConfig;
import com.javatechie.rabbitmq.demo.entity.SimulatorSensor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class SpringbootRabbitmqExampleApplication {

	@Autowired
	private RabbitTemplate templateRabbit;

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(SpringbootRabbitmqExampleApplication.class, args);

	}


//	@Value("${SENSOR_ID}") private String sensorId;
    private int noSimulatorSensor=1;
	@Bean
	CommandLineRunner init() {
		return args -> {
			ReadFromFile read = new ReadFromFile();
			List<SimulatorSensor> simulatorSensors =  read.readFromFile();
			for(int s=0;s<simulatorSensors.size();s++){

				System.out.println("message "+noSimulatorSensor+ " sent");
				noSimulatorSensor=noSimulatorSensor+1;

				templateRabbit.convertAndSend(MQConfig.EXCHANGE,
						MQConfig.ROUTING_KEY, simulatorSensors.get(s));
			}
		};
	}

}
