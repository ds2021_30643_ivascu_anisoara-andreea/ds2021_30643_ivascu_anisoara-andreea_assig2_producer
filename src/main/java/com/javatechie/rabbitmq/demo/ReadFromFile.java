package com.javatechie.rabbitmq.demo;


import com.javatechie.rabbitmq.demo.entity.SimulatorSensor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.io.File;
import java.io.FileNotFoundException;
import java.time.Clock;
import java.time.Instant;
import java.util.*;


@Component
public class ReadFromFile {

   private static final UUID SENSOR_ID =UUID.fromString("5c2494a3-1140-4c7a-991a-a1a2561c6bc2");
 //@Value("${sensor_Id}")
   // private static String sensor_id;
    //private static final UUID SENSOR_ID=sensor_id;*/

    //@Value("${SENSOR_ID}") private String sensorId;
    //private int noSimulatorSensor=1;


    static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs
    int minutes=0;

   /* @Value("${sensor_id}")  UUID sensor_id;

    public  setSensor_id(UUID SENSOR_ID) {
        this.sensor_id = SENSOR_ID;
    }*/

    // UUID SENSOR_ID =UUID.fromString(sensor_id);

    int MINUTES = 1; // The delay in minutes
    //@Value("${server_port}") String SENSOR;


    public  List<SimulatorSensor> readFromFile() {
        //System.out.println(SENSOR);
        File f = new File("sensor.csv");

        List<SimulatorSensor> sensor = new ArrayList<>();
       // UUID SENSOR_ID =UUID.fromString(sensor_id);



        try {
            //System.out.println(SENSOR_ID);
            Scanner scanner = new Scanner(f);
            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();
                double val_sensor = Double.parseDouble(line);
                //** System.out.println(line);
                //LocalTime time2=time1.plusMinutes(10);
                //** System.out.println(time1);

                //Thread.sleep(1000 * 30 * MINUTES);


                //  data.add(new SimulatorSensor(SENSOR_ID,);
                //LocalTime time1 = LocalTime.now();
                //Calendar now = Calendar.getInstance();
               // now.add(Calendar.MINUTE, 10);
               // Date teenMinutesFromNow = now.getTime();

                Calendar calendar = Calendar.getInstance();
                //System.out.println("Current Date = " + calendar.getTime());
                // Add 10 minutes to current date
                calendar.add(Calendar.MINUTE, 10*minutes);
                calendar.add(Calendar.HOUR, 2);
               // System.out.println("Updated Date = " + calendar.getTime());
                SimulatorSensor newSensorValue=new SimulatorSensor(SENSOR_ID,calendar.getTime(),val_sensor);
                sensor.add(newSensorValue);
                minutes=minutes+1;
                //SimulatorSensor ss=new SimulatorSensor();
                //System.out.println("aici "+sensorId);

            }
            scanner.close();
        } catch (FileNotFoundException  e) {
            e.printStackTrace();
        }
        return sensor;
    }


}